import { Loan } from './loan.model'

export interface Book {
    id: number;
    name: string;
    image: string;
    userId: number;
    loan: Loan | false;
    description?: string;
}

export enum Availability {
    AVAILABLE = 'available',
    UNAVAILABLE = 'unavailable'
}