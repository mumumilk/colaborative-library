export interface Loan {
    userId: number;
    date: string;
}