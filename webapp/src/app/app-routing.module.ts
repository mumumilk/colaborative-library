import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { LayoutComponent } from './shared/components/layout/layout.component';
import { AuthLayoutComponent } from './auth/auth-layout.component';
import { HomeComponent } from './book/home/home.component';
import { BooksLayoutComponent } from './book/books-layout.component';
import { LoansComponent } from './book/loans/loans.component';
import { MyBooksComponent } from './book/my-books/my-books.component';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'auth/login',
                pathMatch: 'full'
            },
            {
                path: 'auth',
                component: AuthLayoutComponent,
                children: [
                    {
                        path: '',
                        redirectTo: 'login',
                        pathMatch: 'full'
                    },
                    {
                        path: 'login',
                        component: LoginComponent
                    },
                    {
                        path: 'register',
                        component: RegisterComponent
                    }
                ]
            },
            {
                path: 'books',
                component: BooksLayoutComponent,
                canActivate: [AuthGuard],
                children: [
                    {
                        path: 'home',
                        component: HomeComponent
                    },
                    {
                        path: 'loans',
                        component: LoansComponent
                    },
                    {
                        path: 'my-books',
                        component: MyBooksComponent
                    }
                ]
            }
        ]       
    },
    {
        path: '**',
        redirectTo: 'books/home'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
