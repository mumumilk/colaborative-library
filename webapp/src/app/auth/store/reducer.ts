import { createReducer, on, Action } from '@ngrx/store';
import User from 'src/app/shared/models/user.model';
import * as AuthActions from './actions';

export interface AuthState {
    authenticatedUser: User;
    users: User[];
    errorMessage: string;
}

const initialState: AuthState = {
    authenticatedUser: null,
    users: [],
    errorMessage: null
}

const authReducer = createReducer(
    initialState,
    on(AuthActions.loginSuccess, (state, { user }) => ({ ...state, authenticatedUser: user })),
    on(AuthActions.loginFail, (state, { errorMessage }) => ({ ...state, errorMessage })),
    on(AuthActions.registerSuccess, (state, { user }) => ({ ...state, authenticatedUser: user })),
    on(AuthActions.loadUsersSuccess, (state, { users }) => ({ ...state, users })),
    on(AuthActions.registerSuccess, (state, { user }) => ({ ...state, authenticatedUser: user }))
);

export function reducer(state: AuthState | undefined, action: Action) {
    return authReducer(state, action);
}