import { createSelector, createFeatureSelector } from '@ngrx/store';
import { AuthState } from './reducer';

export const authStateSelector = createFeatureSelector('Auth');

export const usersSelector = createSelector(authStateSelector, (state: AuthState) => state.users);

export const authenticatedUserSelector = createSelector(authStateSelector, (state: AuthState) => state.authenticatedUser);