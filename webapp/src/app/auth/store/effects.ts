import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, Effect } from '@ngrx/effects';
import { UserService } from 'src/app/core/services/user.service';
import { AuthActionTypes, loadUsersSuccess, loginSuccess, loginFail, registerSuccess } from './actions';
import { map, switchMap, withLatestFrom, tap, catchError } from 'rxjs/operators'
import { Store } from '@ngrx/store';
import { usersSelector } from './selectors';
import User from 'src/app/shared/models/user.model';
import { Router } from '@angular/router';

@Injectable()
export class AuthEffects {

    constructor(
        private actions$: Actions,
        private userService: UserService,
        private store: Store<any>,
        private router: Router
    ) { }

    loadUsers$ = createEffect(() => this.actions$.pipe(
        ofType(AuthActionTypes.LOAD_USERS),
        switchMap(() => this.userService.getAll()),
        map((users) => loadUsersSuccess({users}))
    ));

    login$ = createEffect(() => this.actions$.pipe(
        ofType(AuthActionTypes.LOGIN),
        withLatestFrom(this.store.select(usersSelector)),
        map(([act, users]:[any, User[]]) => {
            const userFound = users.find(u => u.email == act.user.email && u.password == act.user.password);
            if (userFound) {
                return loginSuccess({user: userFound});
            } else {
                return loginFail({errorMessage: 'User not found'})
            }
        })
    ));

    successfulAuth$ = createEffect(() => this.actions$.pipe(
        ofType(AuthActionTypes.LOGIN_SUCCESS, AuthActionTypes.REGISTER_SUCCESS),
        tap((act) => this.router.navigate(['/books/home']))
    ), {dispatch: false});

    register$ = createEffect(() => this.actions$.pipe(
        ofType(AuthActionTypes.REGISTER),
        tap((act) => console.log(act)),
        switchMap((act: any) => this.userService.add(act.user)),
        map((user) => registerSuccess({user}))
    ));

}