import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable, of } from 'rxjs';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthEffects } from './effects';
import { AuthActionTypes } from './actions';
import { Action } from '@ngrx/store';
import { UserService } from 'src/app/core/services/user.service';
import { usersSelector } from './selectors';
import { Router } from '@angular/router';

describe('Auth Effects', () => {

    let actions$: Observable<Action>;
    let effects: AuthEffects;
    let userService: UserService;
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                provideMockActions(() => actions$),
                AuthEffects,
                provideMockStore({
                    selectors: [
                        {
                            selector: usersSelector,
                            value: [
                                { name: 'test', password: 'test' }
                            ]
                        }
                    ]
                }),
                { provide: UserService, useValue: { getAll: () => { }, add: () => { } } }
            ],
            imports: [
                RouterTestingModule.withRoutes([])
            ]
        });

        effects = TestBed.get<AuthEffects>(AuthEffects);
        userService = TestBed.get(UserService);
        router = TestBed.get(Router);
    });

    it('receives a load users and dispatch a load users success', (done) => {
        spyOn(userService, 'getAll').and.returnValue(of([]));

        actions$ = of({ type: AuthActionTypes.LOAD_USERS });

        effects.loadUsers$.subscribe((act) => {
            expect(act.type).toBe(AuthActionTypes.LOAD_USERS_SUCCESS);
            done();
        });
    });

    it('dispatches a successful login after finding a matching user', (done) => {
        actions$ = of({ type: AuthActionTypes.LOGIN, user: { name: 'test', password: 'test' } });

        effects.login$.subscribe((act) => {
            expect(act.type).toBe(AuthActionTypes.LOGIN_SUCCESS);
            done();
        });
    });

    it('dispatches a failed login action after not finding a matching user', (done) => {
        actions$ = of({ type: AuthActionTypes.LOGIN, user: { name: 't', password: 't' } });

        effects.login$.subscribe((act) => {
            expect(act.type).toBe(AuthActionTypes.LOGIN_FAIL);
            done();
        });
    });

    it('navigates to books home page after a successful authentication', () => {
        actions$ = of({ type: AuthActionTypes.LOGIN_SUCCESS });

        const navigatorSpy = spyOn(router, 'navigate');

        effects.successfulAuth$.subscribe();

        expect(navigatorSpy).toHaveBeenCalledWith(['/books/home']);
    });

    it('dispatches a successful register action after adding a new user', (done) => {
        spyOn(userService, 'add').and.returnValue(of({ name: 't', email: 't@t.t', password: '123', id: 1 }));

        actions$ = of({ type: AuthActionTypes.REGISTER });

        effects.register$.subscribe((act) => {
            expect(act.type).toBe(AuthActionTypes.REGISTER_SUCCESS);
            done();
        });
    });

});
