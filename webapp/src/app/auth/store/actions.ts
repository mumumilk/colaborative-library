import { createAction, props } from '@ngrx/store';
import User from 'src/app/shared/models/user.model';

export enum AuthActionTypes {
    LOGIN = '[Auth] Login',
    LOGIN_SUCCESS = '[Auth] Login Success',
    LOGIN_FAIL = '[Auth] Login Fail',
    REGISTER = '[Auth] Register',
    REGISTER_SUCCESS = '[Auth] Register Success',
    LOAD_USERS = '[Auth] Load Users',
    LOAD_USERS_SUCCESS = '[Auth] Load Users Success'
}

export const loadUsers = createAction(
    AuthActionTypes.LOAD_USERS
);

export const loadUsersSuccess = createAction(
    AuthActionTypes.LOAD_USERS_SUCCESS,
    props<{users: User[]}>()
)

export const login = createAction(
    AuthActionTypes.LOGIN,
    props<{user: User}>()
);

export const loginSuccess = createAction(
    AuthActionTypes.LOGIN_SUCCESS,
    props<{user: User}>()
);

export const loginFail = createAction(
    AuthActionTypes.LOGIN_FAIL,
    props<{errorMessage: string}>()
);

export const register = createAction(
    AuthActionTypes.REGISTER,
    props<{user: User}>()
);

export const registerSuccess = createAction(
    AuthActionTypes.REGISTER_SUCCESS,
    props<{user: User}>()
);