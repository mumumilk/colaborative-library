import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { authenticatedUserSelector } from './store/selectors';
import { map } from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {
    constructor(
        private store: Store<any>,
        private router: Router
    ) {}

    canActivate(): Observable<boolean> {
        return this.store.select(authenticatedUserSelector)
            .pipe(
                map((user) => {
                    if (!user) {
                        this.router.navigate(['/auth/login'])
                    }
                    return !!user
                })
            );
    }
}