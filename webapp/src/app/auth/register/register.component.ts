import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { register } from '../store/actions';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styles: ['form * { padding-top: 10px }']
})
export class RegisterComponent implements OnInit {

    public form: FormGroup;

    constructor(private store: Store<any>) { }

    ngOnInit() {
        this.form = new FormGroup({
            name: new FormControl('', Validators.required),
            email: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', Validators.required)
        });
    }

    submit(): void {
        this.store.dispatch(register({user: this.form.value}));
    }
}
