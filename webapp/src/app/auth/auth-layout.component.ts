import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-auth-layout',
    template: `
    <section fxLayout="column" fxLayoutAlign="center center" [style.height.%]="100">
        <h1>{{companyName}}</h1>
        <div class="card" >
            <router-outlet></router-outlet>
        </div>
    </section>
    `,
    styles: ['.card { min-width: 30vw; padding: 24px }']
})
export class AuthLayoutComponent {
    companyName: string = 'Bookstore'
}
