import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthLayoutComponent } from './auth-layout.component'
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { reducer } from './store/reducer';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from './store/effects';

@NgModule({
    declarations: [
        LoginComponent,
        RegisterComponent,
        AuthLayoutComponent
    ],
    exports: [
        LoginComponent,
        RegisterComponent,
        AuthLayoutComponent
    ],
    imports: [
        CommonModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        RouterModule,
        StoreModule.forFeature('Auth', reducer),
        EffectsModule.forFeature([AuthEffects])
    ]
})
export class AuthModule { }
