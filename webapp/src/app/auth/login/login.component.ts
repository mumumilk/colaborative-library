import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { login } from '../store/actions';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styles: ['form * { padding-top: 10px }']
})
export class LoginComponent implements OnInit {
    public form: FormGroup;

    constructor(private store: Store<any>) { }

    ngOnInit() {
        this.form = new FormGroup({
            email: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', Validators.required)
        });
    }

    submit(): void {
        this.store.dispatch(login({user: this.form.value}));    
    }
}
