import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Book } from 'src/app/shared/models/book.model';

@Component({
    selector: 'app-book-detail',
    templateUrl: './book-detail.component.html'
})
export class BookDetailComponent {
    book: Book = null;

    constructor(public bsModalRef: BsModalRef) { }
}
