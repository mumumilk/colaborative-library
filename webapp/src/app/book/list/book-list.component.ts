import { Component, OnInit, Input, Output, EventEmitter, TemplateRef } from '@angular/core';
import { Book } from 'src/app/shared/models/book.model';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { BookDetailComponent } from '../detail/book-detail.component';

@Component({
    selector: 'app-book-list',
    templateUrl: './book-list.component.html',
    styleUrls: ['./book-list.component.sass']
})
export class BookListComponent implements OnInit {
    @Input() books: Book[];
    @Input() showAction: boolean = false;
    @Output() actionClicked: EventEmitter<Book> = new EventEmitter<Book>();
    modalRef: BsModalRef;

    constructor(private modalService: BsModalService) { }

    ngOnInit() {
    }

    onActionClick(book: Book) {
        this.actionClicked.emit(book);
    }

    openDetails(book: Book) {
        const initialState = {
            book
        };
        this.modalRef = this.modalService.show(BookDetailComponent, {
            initialState,
            class: 'modal-lg'
        });
        this.modalRef.content.closeBtnName = 'Close';
    }
}