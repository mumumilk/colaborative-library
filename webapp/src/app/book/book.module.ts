import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { BooksLayoutComponent } from './books-layout.component';
import { SharedModule } from '../shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { reducer } from './store/reducer';
import { EffectsModule } from '@ngrx/effects';
import { BookEffects } from './store/effects';
import { LoansComponent } from './loans/loans.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BookListComponent } from './list/book-list.component';
import { BookDetailComponent } from './detail/book-detail.component';
import { MyBooksComponent } from './my-books/my-books.component';
import { AddBookComponent } from './add/add-book.component';

@NgModule({
    declarations: [
        HomeComponent,
        BooksLayoutComponent,
        LoansComponent,
        BookListComponent,
        BookDetailComponent,
        MyBooksComponent,
        AddBookComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        FlexLayoutModule,
        CollapseModule,
        ButtonsModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        StoreModule.forFeature('Book', reducer),
        EffectsModule.forFeature([BookEffects])
    ],
    exports: [
        HomeComponent,
        BooksLayoutComponent,
        LoansComponent,
        MyBooksComponent
    ],
    entryComponents: [
        BookDetailComponent,
        AddBookComponent
    ]
})
export class BookModule { }
