import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { authenticatedUserSelector } from 'src/app/auth/store/selectors';
import { take } from 'rxjs/operators';
import { addBook } from '../store/actions';

@Component({
    selector: 'app-add-book',
    templateUrl: './add-book.component.html'
})
export class AddBookComponent implements OnInit {
    form: FormGroup;
    constructor(
        public bsModalRef: BsModalRef,
        private store: Store<any>
    ) { }

    ngOnInit(): void {
        this.form = new FormGroup({
            name: new FormControl('', Validators.required),
            description: new FormControl(''),
            image: new FormControl('', Validators.required)
        });
    }

    submit() {
        this.store.select(authenticatedUserSelector).pipe(take(1))
            .subscribe((user) => {
                this.store.dispatch(addBook({
                    book: {
                        ...this.form.value,
                        loan: false,
                        userId: user.id
                    }
                }))
                this.bsModalRef.hide();
            });
    }
}
