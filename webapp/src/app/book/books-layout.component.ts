import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-books-layout',
    templateUrl: './books-layout.component.html',
    styleUrls: ['./books-layout.component.sass']
})
export class BooksLayoutComponent implements OnInit {
    isCollapsed = false;

    constructor() {
    }


    ngOnInit(): void { }
}
