import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Book, Availability } from 'src/app/shared/models/book.model';
import { booksSelector } from '../store/selectors';
import { loadBooks, borrowBook } from '../store/actions';
import { authenticatedUserSelector } from 'src/app/auth/store/selectors';
import { take, filter, tap, map } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.sass'],
    providers: [DatePipe]
})
export class HomeComponent implements OnInit {
    books$: Observable<Book[]>;
    availability: string = Availability.AVAILABLE;

    constructor(private store: Store<any>, private datePipe: DatePipe) {
        this.store.dispatch(loadBooks());
    }

    ngOnInit() {
        this.books$ = this.store.select(booksSelector)
            .pipe(map(books => {
                return books.filter((book) => this.filterByAvailability(book))
            }));
    }

    borrowBook(book: Book) {
        this.store.select(authenticatedUserSelector).pipe(take(1)).subscribe((user) => {
            this.store.dispatch(borrowBook({
                book: {
                    ...book,
                    loan: {
                        userId: user.id,
                        date: this.datePipe.transform(new Date(), 'yyyy-MM-dd')
                    }
                }
            }))
        });
    }

    onAvailabilityChange(event) {
        this.store.dispatch(loadBooks());
    }

    filterByAvailability(book) {
        return this.availability === Availability.AVAILABLE
        ? book.loan === false
        : book.loan != false
    }

}