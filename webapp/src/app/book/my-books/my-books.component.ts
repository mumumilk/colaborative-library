import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Book } from 'src/app/shared/models/book.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AddBookComponent } from '../add/add-book.component';
import { Store } from '@ngrx/store';
import { myBooksSelector } from '../store/selectors';

@Component({
    selector: 'app-my-books',
    templateUrl: './my-books.component.html',
    styleUrls: ['./my-books.component.sass']
})
export class MyBooksComponent implements OnInit {
    books$: Observable<Book[]> = of([])
    modalRef: BsModalRef;

    constructor(private modalService: BsModalService, private store: Store<any>) { }

    ngOnInit(): void {
        this.books$ = this.store.select(myBooksSelector);
    }

    openForm(book: Book) {
        const initialState = {

        };
        this.modalRef = this.modalService.show(AddBookComponent, {
            initialState,
            backdrop: 'static'
        });
        this.modalRef.content.closeBtnName = 'Close';
    }
}
