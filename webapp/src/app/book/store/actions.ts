import { createAction, props } from '@ngrx/store';
import { Book } from 'src/app/shared/models/book.model';

export enum BookActionTypes {
    LOAD_BOOKS = '[Book] Load Books',
    LOAD_BOOKS_SUCCESS = '[Book] Load Books Success',
    BORROW_BOOK = '[Book] Borrow Book',
    LOAD_LOANS = '[Book] Load Loans',
    LOAD_LOANS_SUCCESS = '[Book] Load Loans Success',
    ADD_BOOK = '[Book] Add Book',
    ADD_BOOK_SUCCESS = '[Book] Add Book Success'
}

export const loadBooks = createAction(
    BookActionTypes.LOAD_BOOKS
);

export const loadBooksSuccess = createAction(
    BookActionTypes.LOAD_BOOKS_SUCCESS,
    props<{books: Book[]}>()
);

export const borrowBook = createAction(
    BookActionTypes.BORROW_BOOK,
    props<{book: Book}>()
);

export const loadLoans = createAction(
    BookActionTypes.LOAD_LOANS,
    props<{userId: number}>()
);

export const loadLoansSuccess = createAction(
    BookActionTypes.LOAD_LOANS_SUCCESS,
    props<{books: Book[]}>()
);

export const addBook = createAction(
    BookActionTypes.ADD_BOOK,
    props<{book: Book}>()
);

export const addBookSuccess = createAction(
    BookActionTypes.ADD_BOOK_SUCCESS,
    props<{book: Book}>()
);