import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable, of } from 'rxjs';
import { TestBed } from '@angular/core/testing';
import { BookEffects } from './effects';
import { BookActionTypes } from './actions';
import { Action } from '@ngrx/store';
import { BookService } from 'src/app/core/services/book.service';
import { Book } from 'src/app/shared/models/book.model';

describe('Book Effects', () => {

    let actions$: Observable<Action>;
    let effects: BookEffects;
    let bookService: BookService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                provideMockActions(() => actions$),
                BookEffects,
                provideMockStore({}),
                {
                    provide: BookService,
                    useValue: {
                        getAll: () => { },
                        update: (book) => { },
                        add: (book) => { }
                    }
                }
            ]
        });

        effects = TestBed.get<BookEffects>(BookEffects);
        bookService = TestBed.get(BookService);
    });

    it('receives a load books and dispatch a load books success', (done) => {
        spyOn(bookService, 'getAll').and.returnValue(of([]));

        actions$ = of({ type: BookActionTypes.LOAD_BOOKS });

        effects.loadBooks$.subscribe((act) => {
            expect(act.type).toBe(BookActionTypes.LOAD_BOOKS_SUCCESS);
            done();
        });
    });

    it('updates the book after borrowing it', () => {
        actions$ = of({
            type: BookActionTypes.BORROW_BOOK, book: {
                id: 1,
                name: 'test',
                image: 'url',
                loan: false,
                userId: 1
            }
        });

        const updateSpy = spyOn(bookService, 'update');

        effects.borrowBook$.subscribe();

        expect(updateSpy).toHaveBeenCalled();

    });

    it('adds a book and dispatches a successful action', (done) => {
        const book: Book = {
            id: 1,
            name: 'test',
            image: 'asdasd',
            userId: 1,
            loan: false
        };
        spyOn(bookService, 'add').and.returnValue(of(book));

        actions$ = of({ type: BookActionTypes.ADD_BOOK, book });

        effects.addBook$.subscribe((act) => {
            expect(act.type).toBe(BookActionTypes.ADD_BOOK_SUCCESS);
            done();
        });
    });

});
