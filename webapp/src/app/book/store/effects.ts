import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { BookService } from 'src/app/core/services/book.service';
import { BookActionTypes, loadBooksSuccess, loadLoansSuccess, addBookSuccess } from './actions';
import { map, switchMap } from 'rxjs/operators'

@Injectable()
export class BookEffects {

    constructor(
        private actions$: Actions,
        private bookService: BookService
    ) { }

    loadBooks$ = createEffect(() => this.actions$.pipe(
        ofType(BookActionTypes.LOAD_BOOKS),
        switchMap(() => this.bookService.getAll()),
        map((books) => loadBooksSuccess({books}))
    ));

    borrowBook$ = createEffect(() => this.actions$.pipe(
        ofType(BookActionTypes.BORROW_BOOK),
        switchMap((act: any) => this.bookService.update(act.book))
    ), {dispatch: false});

    loadLoans$ = createEffect(() => this.actions$.pipe(
        ofType(BookActionTypes.LOAD_LOANS),
        switchMap((act: any) => this.bookService.loans(act.userId)),
        map((books) => loadLoansSuccess({books}))
    ));

    addBook$ = createEffect(() => this.actions$.pipe(
        ofType(BookActionTypes.ADD_BOOK),
        switchMap((act: any) => this.bookService.add(act.book)),
        map((book) => addBookSuccess({book}))
    ));
}