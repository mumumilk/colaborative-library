import { createReducer, on, Action } from '@ngrx/store';
import * as BooksActions from './actions';
import { Book } from 'src/app/shared/models/book.model';

export interface BookState {
    books: Book[],
    loans: Book[]
}

const initialState: BookState = {
    books: [],
    loans: []
}

const booksReducer = createReducer(
    initialState,
    on(BooksActions.loadBooksSuccess, (state, { books }) => ({ ...state, books })),
    on(BooksActions.borrowBook, (state, { book }) => ({ 
        ...state,
        books: state.books.map(
            (b) => b.id === book.id ? book : b
        )
    })),
    on(BooksActions.loadLoansSuccess, (state, { books }) => ({ ...state, loans: books })),
    on(BooksActions.addBook, (state, { book }) => ({ ...state, books: [...state.books, book]}))
);

export function reducer(state: BookState | undefined, action: Action) {
    return booksReducer(state, action);
}