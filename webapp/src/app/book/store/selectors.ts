import { createSelector, createFeatureSelector } from '@ngrx/store';
import { BookState } from './reducer';
import { authenticatedUserSelector } from 'src/app/auth/store/selectors';
import { Book } from 'src/app/shared/models/book.model';
import User from 'src/app/shared/models/user.model';

export const bookStateSelector = createFeatureSelector('Book');

export const booksSelector = createSelector(bookStateSelector, (state: BookState) => state.books);

export const loansSelector = createSelector(bookStateSelector, (state: BookState) => state.loans);

export const myBooksSelector = createSelector(
    [booksSelector, authenticatedUserSelector],
    (books: Book[], user: User) => books.filter(book => book.userId === user.id)
);