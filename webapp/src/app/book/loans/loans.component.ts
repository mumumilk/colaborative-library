import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { combineLatest, take } from 'rxjs/operators';
import { loansSelector } from '../store/selectors';
import { authenticatedUserSelector } from 'src/app/auth/store/selectors';
import { loadLoans } from '../store/actions';
import { Observable } from 'rxjs';
import { Book } from 'src/app/shared/models/book.model';

@Component({
    selector: 'app-loans',
    templateUrl: './loans.component.html',
    styleUrls: ['./loans.component.sass']
})
export class LoansComponent implements OnInit {
    books$: Observable<Book[]>;

    constructor(private store: Store<any>) {
        this.store.select(authenticatedUserSelector).pipe(take(1)).subscribe((user) => {
            this.store.dispatch(loadLoans({userId: user.id}));
        });
    }

    ngOnInit() {
        this.books$ = this.store.select(loansSelector);
    }

}
