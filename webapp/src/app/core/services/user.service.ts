import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import User from 'src/app/shared/models/user.model';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private http: HttpClient) { }

    public getAll(): Observable<User[]>{
        return this.http.get<User[]>(`${environment.api}/users`);
    }

    public add(user: User) {
        return this.http.post<User>(`${environment.api}/users`, user);
    }
}
