import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { BookService } from './book.service';
import { Book } from 'src/app/shared/models/book.model';

describe('BookService', () => {
    let service: BookService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [BookService]
        });

        service = TestBed.get(BookService);
        httpMock = TestBed.get(HttpTestingController);
    });

    it('should be created', () => {
        const service: BookService = TestBed.get(BookService);
        expect(service).toBeTruthy();
    });

    it('should fetch all books', () => {
        service.getAll().subscribe((data: Book[]) => {
            expect(data.length).toBe(1);
        });

        const req = httpMock.expectOne(`http://localhost:3000/books`, 'books endpoint');
        expect(req.request.method).toBe('GET');

        req.flush([
            {
                id: 1,
                name: 'test',
                image: 'url',
                loan: false,
                userId: 1
            }
        ]);

        httpMock.verify();
    });

    it('should update a book', () => {
        const book: Book = {
            id: 1,
            name: 'test',
            image: 'url',
            loan: false,
            userId: 1
        };
        
        service.update(book).subscribe((data: Book) => {
            expect(data.name).toBe('test');
        });
        const req = httpMock.expectOne(`http://localhost:3000/books/1`, 'books endpoint');
        expect(req.request.method).toBe('PUT');

        req.flush(
            {
                id: 1,
                name: 'test',
                image: 'url',
                loan: false,
                userId: 1
            }
        );

        httpMock.verify();
    });

    it('should add a book', () => {
        const book: Book = {
            id: 1,
            name: 'test',
            image: 'url',
            loan: false,
            userId: 1
        };
        
        service.add(book).subscribe((data: Book) => {
            expect(data.name).toBe('test');
        });
        const req = httpMock.expectOne(`http://localhost:3000/books`, 'books endpoint');
        expect(req.request.method).toBe('POST');

        req.flush(
            {
                id: 1,
                name: 'test',
                image: 'url',
                loan: false,
                userId: 1
            }
        );

        httpMock.verify();
    });
});
