import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Book } from 'src/app/shared/models/book.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class BookService {

    constructor(private http: HttpClient) { }

    public getAll(): Observable<Book[]> {
        return this.http.get<Book[]>(`${environment.api}/books`);
    }

    public update(book: Book): Observable<Book> {
        return this.http.put<Book>(`${environment.api}/books/${book.id}`, book);
    }

    public loans(userId: number): Observable<Book[]> {
        let params = new HttpParams().set('loan.userId', userId.toString());

        return this.http.get<Book[]>(`${environment.api}/books`, { params });
    }

    public add(book: Book): Observable<Book> {
        return this.http.post<Book>(`${environment.api}/books`, book);
    }
}
