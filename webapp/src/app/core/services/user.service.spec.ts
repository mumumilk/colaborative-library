import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { UserService } from './user.service';
import User from 'src/app/shared/models/user.model';

describe('UserService', () => {
    let service: UserService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [UserService]
        });

        service = TestBed.get(UserService);
        httpMock = TestBed.get(HttpTestingController);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should fetch all users', () => {
        service.getAll().subscribe((data: User[]) => {
            expect(data.length).toBe(2);
        });

        const req = httpMock.expectOne(`http://localhost:3000/users`, 'users endpoint');
        expect(req.request.method).toBe('GET');

        req.flush([
            {
                name: 'John Doe',
                id: 45
            },
            {
                name: 'Doe John',
                id: 50
            }
        ]);

        httpMock.verify();
    });

    it('should add a user', () => {
        const user: User = {
            name: 'teste',
            password: 'teste',
            email: 'teste',
            id: 1
        };
        service.add(user).subscribe((data: User) => {
            expect(data.id).toBe(1);
        });

        const req = httpMock.expectOne(`http://localhost:3000/users`, 'users endpoint');
        expect(req.request.method).toBe('POST');

        req.flush({
            id: 1
        });

        httpMock.verify();
    });


});
