import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { loadUsers } from './auth/store/actions';

@Component({
    selector: 'app-root',
    template: `
        <router-outlet></router-outlet>
    `
})
export class AppComponent {
    constructor(private store: Store<any>) {
        this.store.dispatch(loadUsers())
    }
}
