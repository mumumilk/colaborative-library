# Welcome to Webapp

This is an Angular-made application to consume books API.

## How to run

### 1 -	install dependencies
`$ npm install`

### 2 - start server
Go to server's folder `$ cd ../server` and follow readme instructions to run it.

### 3 - configure server's URL

If your server runs in a different location rather than `localhost:3000`, you'll need to change it's URl in the webapp application. To do so, go to `webapp/src/environments/` and edit the file `environment.ts`

### 4 - start webapp application
`$ npm start`

## How to test
`$ npm test`